# Web demos in Zig

These example projects are Web demos written in Zig, compiled to WebAssembly. These are inspired or ported from the web demos created by Cliff L. Biffle. Find the original blog posts about his Web Demos [here](https://cliffle.com/p/web-demos/).

## Zig version

I use version 0.10.1.

## Demos

Find a short explanation of each demo below:

## Useful links

* [https://cliffle.com/blog/bare-metal-wasm/]
* [Zig on WebAssembly](https://gist.github.com/josephg/873a21d4558fd69aeccea19c3df96672)
* [WebAssembly with Zig - Part 1](https://dev.to/sleibrock/webassembly-with-zig-part-1-4onm)
* [Failed to define the target at build.zig](https://stackoverflow.com/questions/76515618/failed-to-define-the-target-at-build-zig)
* [Zig Build](https://ikrima.dev/dev-notes/zig/zig-build/)

