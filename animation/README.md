# Readme animation

Animation example that for a minimal WASM module in Zig with graphics "output", that can be used in the Browser. It also provides a minimal index.html.

## Building

Build with zig compiler 0.10.1:

```shell
zig build -Drelease-small=true
# always create small modules ;)
```

## Using

The build step should already install the library and copy it to the `static` folder. So one must just serve this folder with a static web server and use the browser to browse the page. Example:

* Start static web server with python version 3

```shell
python -m http.server -d static
```

Open browser and type in address `localhost:8000`. You should see a magenta square on the web page.

