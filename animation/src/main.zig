const width: usize = 600;
const height: usize = 600;
const length: usize = width * height;

export var buffer: [length]u32 = [_]u32{0} ** length;

const Atomic = @import("std").atomic.Atomic;
const Ordering = @import("std").atomic.Ordering;
const AtomicU32 = Atomic(u32);

var frame = AtomicU32.init(0);

export fn go() callconv(.C) void {
    // increment the frame number
    const current_frame_number = frame.load(.Monotonic);
    const new_frame_number = current_frame_number + 1;
    // Should not happen in the demo, but in a different setting another concurrent thread may already have incremented in the frame number
    const opt_frame_number = frame.compareAndSwap(current_frame_number, new_frame_number, .Monotonic, .Monotonic);
    var frame_number = new_frame_number;
    // If the swap was not necessary, i.e. the frame_number that was loaded differs from the actual frame number, use the number stored in the atomic
    if (opt_frame_number) |n| {
        frame_number = n;
    }
    var x: usize = 0;
    while (x < width) : (x += 1) {
        var y: usize = 0;
        while (y < height) : (y += 1) {
            buffer[y * width + x] = frame_number +% (@as(u32, x ^ y) | 0xFF_00_00_00);
        }
    }
}
