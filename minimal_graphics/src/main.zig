const width: usize = 600;
const height: usize = 600;
const length: usize = width * height;

export var buffer: [length]u32 = [_]u32{0} ** length;

export fn go() callconv(.C) void {
    for (buffer) |*pixel| {
        pixel.* = 0xFF_FF_00_FF;
    }
}
