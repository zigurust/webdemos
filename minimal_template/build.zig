const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const module_name = "minimal_graphics";
    const mode = b.standardReleaseOptions();
    // we want to create a wasm32 freestanding target by default
    const wasm_target = std.zig.CrossTarget{ .os_tag = .freestanding, .cpu_arch = .wasm32 };
    const target = b.standardTargetOptions(.{ .default_target = wasm_target });

    const lib = b.addSharedLibrary(module_name, "src/main.zig", std.build.LibExeObjStep.SharedLibKind.unversioned);
    lib.setTarget(target);
    lib.setBuildMode(mode);
    lib.install();

    // copy artifact into static folder
    const path_to_module = "./zig-out/lib/" ++ module_name ++ ".wasm";
    const cp_cmd = b.addSystemCommand(&[_][]const u8{ "cp", path_to_module, "./static/" });
    b.getInstallStep().dependOn(&cp_cmd.step);

    const main_tests = b.addTest("src/main.zig");
    main_tests.setBuildMode(mode);

    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&main_tests.step);
}
