const width: usize = 600;
const height: usize = 600;
const length: usize = width * height;

export var buffer: [length]u32 = [_]u32{0} ** length;

export fn go() callconv(.C) void {
    var x: usize = 0;
    while (x < width) : (x += 1) {
        var y: usize = 0;
        while (y < height) : (y += 1) {
            buffer[y * width + x] = @as(u32, x ^ y) | 0xFF_00_00_00;
        }
    }
}
